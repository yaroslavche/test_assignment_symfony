<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /**
     * ProductRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param Request $request
     * @param int $maxResults
     * @return Product[] Returns an array of Product objects
     * @throws \Exception
     */
    public function findByFilter(array $filter = [], array $sort = [], int $maxResults = 12, int $page = 1)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        if (!empty($sort)) {
            $sortKeys = array_keys($sort);
            $sortBy = $sortKeys[0];
            $sortDir = $sort[$sortBy];
            $queryBuilder->orderBy(sprintf('p.%s', $sortBy), $sortDir);
        }

        $priceFrom = $filter['priceFrom'] ?? null;
        $priceTo = $filter['priceTo'] ?? null;

        // priceFrom > 0 && priceTo > 0 && priceTo > priceFrom
        if ($priceFrom > 0 && $priceTo > $priceFrom) {
            $queryBuilder
                ->andWhere('p.price between :priceFrom and :priceTo')
                ->setParameter('priceFrom', $priceFrom)
                ->setParameter('priceTo', $priceTo)
            ;
        }
        // priceTo not set or invalid - ignore
        elseif ($priceFrom > 0) {
            $queryBuilder
                ->andWhere('p.price > :price')
                ->setParameter('price', $priceFrom)
            ;
        }
        // priceFrom not set
        elseif ($priceTo > 0) {
            $queryBuilder
                ->andWhere('p.price < :price')
                ->setParameter('price', $priceTo)
            ;
        }

        foreach ($filter as $column => $ids) {
            if (in_array($column, ['category', 'master', 'technique']) && count($ids) > 0) {
                $ids = array_map('intval', $ids);
                $queryBuilder
                    ->andWhere(sprintf('p.%s in (:%s)', $column, $column))
                    ->setParameter($column, $ids)
                ;
            }
        }
        return $queryBuilder
            ->setMaxResults($maxResults)
            ->setFirstResult(($page - 1) * $maxResults)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByFilterTotalCount(array $filter = [])
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $queryBuilder->select('count(p.id)');

        $priceFrom = $filter['priceFrom'] ?? null;
        $priceTo = $filter['priceTo'] ?? null;

        // priceFrom > 0 && priceTo > 0 && priceTo > priceFrom
        if ($priceFrom > 0 && $priceTo > $priceFrom) {
            $queryBuilder
                ->andWhere('p.price between :priceFrom and :priceTo')
                ->setParameter('priceFrom', $priceFrom)
                ->setParameter('priceTo', $priceTo)
            ;
        }
        // priceTo not set or invalid - ignore
        elseif ($priceFrom > 0) {
            $queryBuilder
                ->andWhere('p.price >= :price')
                ->setParameter('price', $priceFrom)
            ;
        }
        // priceFrom not set
        elseif ($priceTo > 0) {
            $queryBuilder
                ->andWhere('p.price <= :price')
                ->setParameter('price', $priceTo)
            ;
        }

        foreach ($filter as $column => $value) {
            if (in_array($column, ['category', 'master', 'technique']) && count($value) > 0) {
                $queryBuilder
                    ->andWhere(sprintf('p.%s in (:%s)', $column, $column))
                    ->setParameter($column, $value)
                ;
            }
        }
        return $queryBuilder
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function getMaxPrice()
    {
        $query = $this->createQueryBuilder('p');
        $query->select('MAX(p.price) AS max_price');
        return $query->getQuery()->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
