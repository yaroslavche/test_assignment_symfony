<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Master;
use App\Entity\Product;
use App\Entity\Technique;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CatalogController extends AbstractController
{
    const PRODUCTS_PER_PAGE = 12;

    /**
     * @Route("/", name="catalog")
     */
    public function index()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $masters = $this->getDoctrine()->getRepository(Master::class)->findAll();
        $techniques = $this->getDoctrine()->getRepository(Technique::class)->findAll();
        $maxPrice = $this->getDoctrine()->getRepository(Product::class)->getMaxPrice();
        return $this->render('catalog/index.html.twig', [
            'categories' => $categories,
            'masters' => $masters,
            'techniques' => $techniques,
            'maxPrice' => $maxPrice
        ]);
    }


    /**
     * @param Request $request
     * @Route("/catalog_find_products", name="catalog_find_products")
     */
    public function findProducts(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['error' => 'AJAX only']);
        }

        $sortBy = $request->get('sortBy', 'id');
        $sortDir = $request->get('sortDir', 'DESC');

        $page = (int)$request->get('page', 1);

        $filterPostData = $request->get('filter', []);
        $technique = $filterPostData['technique'] ?? null;
        $master = $filterPostData['master'] ?? null;
        $category = $filterPostData['category'] ?? null;
        $priceFrom = $filterPostData['priceFrom'] ?? null;
        $priceTo = $filterPostData['priceTo'] ?? null;

        $filter = [];
        if (is_array($technique) && count($technique) > 0) {
            $filter['technique'] = $technique;
        }
        if (is_array($master) && count($master) > 0) {
            $filter['master'] = $master;
        }
        if (is_array($category) && count($category) > 0) {
            $filter['category'] = $category;
        }
        if ($priceFrom > 0) {
            $filter['priceFrom'] = $priceFrom;
        }
        if ($priceTo > 0) {
            $filter['priceTo'] = $priceTo;
        }

        if (!in_array($sortBy, ['id', 'price', 'name'])) {
            return new JsonResponse(['code' => 500, 'error' => 'Invalid sort by']);
        }
        if (!in_array($sortDir, ['ASC', 'DESC'])) {
            return new JsonResponse(['code' => 500, 'error' => 'Invalid sort direction']);
        }

        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $productFilterTotalCount = $productRepository->findByFilterTotalCount($filter);
        $products = $productRepository->findByFilter($filter, [$sortBy => $sortDir], static::PRODUCTS_PER_PAGE, $page);
        if (count($products) === 0) {
            return new JsonResponse(['code' => 404, 'error' => 'Not found']);
        }
        $result = [];
        foreach ($products as $product) {
            $result[] = $this->renderView('catalog/product.html.twig', ['product' => $product]);
        }
        return new JsonResponse(['code' => 200, 'resultHtml' => implode('', $result), 'totalCount' => $productFilterTotalCount]);
    }
}
