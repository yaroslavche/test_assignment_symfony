<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Master;
use App\Entity\Technique;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

class AppFixtures extends Fixture implements ContainerAwareInterface
{
    private $container;

    private $categories = [
        'Живопись',
        'Графика',
        'Изделия',
    ];
    private $techniques = [
        'Акварель',
        'Акрил',
        'Масло',
        'Тушь',
        'Карандаш',
    ];

    public function load(ObjectManager $manager)
    {
        $generator = \Faker\Factory::create('ru_RU');
        $populator = new \Faker\ORM\Doctrine\Populator($generator, $manager);
        $populator->addEntity(Category::class, count($this->categories), [
            'name' => function () {
                return array_pop($this->categories);
            }
        ]);
        $populator->addEntity(Technique::class, count($this->techniques), [
            'name' => function () {
                return array_pop($this->techniques);
            }
        ]);
        $populator->addEntity(Master::class, 5, [
            'name' => function () use ($generator) {
                return $generator->name;
            }
        ]);
        $picsFinder = Finder::create()->in($this->container->getParameter('kernel.project_dir') . '/public/img/')->depth(0)->files()->name('pic*');
        $picsIteratorArray = iterator_to_array($picsFinder);
        $pics = [];
        foreach ($picsIteratorArray as $picPath => $picSplFile) {
            $pics[] = '/img/' . $picSplFile->getRelativePathname();
        }
        $populator->addEntity(Product::class, 500, [
            'name' => function () use ($generator) {
                return $generator->sentence(2, 5);
            },
            'price' => function () use ($generator) {
                return $generator->numberBetween(1000, 30000);
            },
            'image' => function () use ($pics) {
                return $pics[mt_rand(0, count($pics) - 1)];
            }
        ]);
        $insertedPKs = $populator->execute();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
